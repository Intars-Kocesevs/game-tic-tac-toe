# 4.november 2021; 6.january 2022, 21-25.january 2022 (Intars Kocesevs)
# Python 3 study;
import itertools    # one function will be used for player's turn switching

# function for game board status output;
# default values: player_input = 0, row_with_change = 0, column_with_change = 0;
def game_board(game_map, player_input = 0, row_with_change = 0,
                column_with_change = 0, call_to_just_display_game_map = False):
    try:
        # before allowing new changes to be made on game map it will be checked
        # whether field was already marked by previous player's choice
        if game_map[row_with_change][column_with_change] != 0:
            print("This position is already occupied. Choose another!")
            return game_map, False  
        # print column numbers respective to the gameboard's size
        column_num = " "
        # calculate size from received gameboard
        game_size_of_current_game = len(game_map)    
        for i in range(game_size_of_current_game):
            column_num += "  " + str(i)
        print(column_num)   
        # if there was input from players then model new situation
        if not call_to_just_display_game_map:
             game_map[row_with_change][column_with_change] = player_input
        # creating iteratively numbered and outputed cycle
        for count, row_in_gameboard in enumerate(game_map):
            print(count, row_in_gameboard)
        return game_map, True
    except IndexError:
        print("Error: you tried to choose row or column outside the range of 0, 1 or 2.")
        return game_map, False
    except Exception as e:
        print (str(e))
        return game_map, False


# function for check up whether on some row, column or diagonals there 
# are identical elements -- a winner check up;
def winner_check_up(current_game):
    for row in current_game:
        # horizontal winner check-up --- 
        # checks current row's all elements whether they are identical and not 0;
        # procedure takes first element of row (row[0]), iterates through row
        # elements, counts number of identical elements and compares this
        # number to the length of a row in a game board:
        if (row.count(row[0]) == len(row) and (0 not in row)):
            print ("Winner on horizontal line --- :", row[0])
            # now, in addition, function will also return logical 'True'
            # which will be targeted to variable game_end in game loop
            return True     # that means return true (that winner has occured)
            
    
    # vertical column winner check-up procedure  |
    for column in range(len(current_game)):    # for number of columns from size of a game
        check_up_vector = []    # initialize variable that will hold column values
        # now start checking column values one by one, column by column
        for row in current_game:
            check_up_vector.append(row[column]) # extracts rows element in respectice column
        # now the count of identical elements in check-up vector is being established
        # as well as tested whether elements represent any respective player's marks;
        # counting is done in reference to first field's value in this vector;
        if check_up_vector.count(check_up_vector[0]) == len(check_up_vector) and check_up_vector[0] != 0:
            print(f"There is a winner on the board. Vertical line |, player {check_up_vector[0]}")
            return True     # that means return true (that winner has occured)
            
    # diagonal-1   \
    # diagonal winner check-up procedure; (from upper-left to lower-right)
    diagonal_up_left__to__down_right = []   # array that will hold according values
    # index-pattern for respective diagonal's fields are 0-0, 1-1, 2-2 etc.
    for indexed_field in range(len(current_game)):     # for game board's length do next:
        diagonal_up_left__to__down_right.append(current_game[indexed_field] [indexed_field])
    if diagonal_up_left__to__down_right.count(diagonal_up_left__to__down_right[0]) == len(diagonal_up_left__to__down_right) and diagonal_up_left__to__down_right[0] != 0:
            print(f"There is a winner on the board. Diagonal line \\, player {diagonal_up_left__to__down_right[0]}")
            return True     # that means return true (that winner has occured)
       
    # diagonal-2   /
    # # diagonal winner check-up procedure; (from lower-left to upper-right);
    # field cell's pattern, for example, for 3x3 board is: 2-0, 1-1, 0-2
    # so there is need to know before what is the length of the game's board;
    # there is a arithmetic pattern to indexes;
    diagonal_bottom_left__to__up_right = []   # array that will hold according values
    length_of_game_board = len(current_game)
    index = length_of_game_board - 1
    indexed_field = 0
    for indexed_field in range(length_of_game_board):
        # get this diagonal's fields values
        diagonal_bottom_left__to__up_right.append (current_game[index][indexed_field])
        index = index - 1
    if diagonal_bottom_left__to__up_right.count(diagonal_bottom_left__to__up_right[0]) == len(diagonal_bottom_left__to__up_right) and diagonal_bottom_left__to__up_right[0] != 0:
            print(f"There is a winner on the board. Diagonal line /, player {diagonal_bottom_left__to__up_right[0]}")
            return True     # that means return true (that winner has occured)

# --------- GAME MAIN LOOP
# the game process loop starts below
interest_in_play = True     # variable that checks if game's quit was asked
players = [1, 2]    # list of players
while interest_in_play:     # while there is interest to continue, do following
    # determine game size
    game_size = int(input("What would be the size of the gameboard? (only natural number)  "))
    if game_size == 3:
        gameboard_situation =  [[0, 0, 0],
                                [0, 0, 0],
                                [0, 0, 0]]
    elif game_size != 3:    # if the gameboard is not standart
        # build the gameboard
        gameboard_situation = []
        for i in range(game_size):
            row = []
            for i in range(game_size):
                row.append(0)
            gameboard_situation.append(row)
        
    max_number_of_fields = game_size * game_size    # will be used to detect stalemate game
    used_fields_counter = 0      # will count used fields
    all_fields_used = False
    game_won = False    # variable that checks whether there was any win
    # now is called a gameboard function in a special mode to just output it;
    # also - to ignore returned second values, a logicals, Python's ' _ ' is used;
    gameboard_situation, _ = game_board(gameboard_situation, call_to_just_display_game_map = True)
    player_switch = itertools.cycle([1, 2])     # will swithc players in each turn
        
    # while there is not any win or stalemate scenario, proceed with game
    while not (game_won and all_fields_used):
        if (used_fields_counter == max_number_of_fields):
            all_fields_used = True
            print("...  stalemate ...")
            game_won = True
            # follows request for replay
            replay_request = input("The game is over, would you like to play again? (y/n)  ")
            if replay_request.lower() == "y":
                print("\n  ..restarting..  \n")
            elif replay_request.lower() == "n":
                print("Game ended.")
                # an update to logical value so it influences main game loop
                interest_in_play = False
            else:
                # in case user inputs wrong symbols, still - ending the game
                print("Not a valid answer/input. The game is being ended.")
                interest_in_play = False
        else:    
            current_player = next(player_switch)
            print(f"Turn for player Nr.{current_player}")
            game_end = False    # logical value that states whether game approached its end
            used_fields_counter += 1
                          
                
        while not (game_end):     # do the following to move game further
            # getting user inputs; Python uses string format when 'input' command is being used
            # user input, thus, must be converted (type cast operation) to numeric (an integer)
            row_choice = int(input("Which row to check (0, 1, 2)?       "))
            column_choice = int(input("Which column to check (0, 1, 2)?    "))
            # now is called the gameboard's function that will update gameboard
            gameboard_situation, game_end = game_board(gameboard_situation, current_player, row_choice, column_choice)
               
        if winner_check_up(gameboard_situation):
            game_won = True
            all_fields_used = True  # switched to True in order to not interfere with restart
            # now, below, user is asked of willing to play another try
            replay_request = input("The game is over, would you like to play again? (y/n)  ")
            if replay_request.lower() == "y":
                print("\n  ..restarting..  \n")
            elif replay_request.lower() == "n":
                print("Game ended.")
                # an update to logical value so it influences main game loop
                interest_in_play = False
            else:
                # in case user inputs wrong symbols, still - ending the game
                print("Not a valid answer/input. The game is being ended.")
                interest_in_play = False
