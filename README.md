# game - tic tac toe

This is my project of a game 'Tic Tac Toe'. Gameboard's size is customizable, allowing to play and experiment not only with classical 3x3 size. Its dynamic size makes the game environment suitable for AI algorithm tests and developments in context of this game for research/training purposes.


## Description
This game project allows to play in a console the Tic Tac Toe game on its classical 3x3 size or larger sizes (for example, 7x7) as well. Custom size is given by user right before start of the game. This code can be basis for developing AI bots / CPU players for this game.

## Installation
Game code may be run through Python 3 interpreter. One easy way to run it is by opening game code file in standart-default IDE 'IDLE' that comes with Python installation by default. Once opened, click "Run" > "Run module". The game should launch.

## Usage
Code has sufficient, basic user's incorrect input handling, so the game is sufficiently playable. In-game choices mostly uses integer numbers for input.

## Roadmap
There will be the same game code in other languages: GNU Octave, C/C++. Also in progress is AI bot/CPU player.

## Authors and acknowledgment
Code is inspired and branched-off from Youtube programming video content/tutorial series creator's "sentdex" approach.
## Project status
In development..
(Intars Kocesevs, 2022)
